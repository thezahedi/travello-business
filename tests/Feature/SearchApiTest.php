<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Availability;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SearchApiTest extends TestCase
{
    use RefreshDatabase;

    public function test_search_api_will_return_200()
    {
        $this->getJson(route('search'))->assertStatus(200);
    }

    public function test_test()
    {
        Product::factory()->has(Availability::factory()->count(3))->count(3)->create();

        dd(Product::with('availabilities')->get()->toArray());
    }
}
